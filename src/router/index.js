import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'HomeView',
    component: () => import('../views/HomeView.vue')
  },
  {
    path: '/skills',
    name: 'SkillsView',
    component: () => import('../views/SkillsView.vue')
  },
  {
    path: '/projects',
    name: 'ProjectsView',
    component: () => import('../views/ProjectsView.vue')
  },
  {
    path: '/diplomas',
    name: 'DiplomasView',
    component: () => import('../views/DiplomasView.vue')
  },
  {
    path: '/about',
    name: 'AboutView',
    component: () => import('../views/AboutView.vue')
  },
  {
    path: '/contact',
    name: 'ContactView',
    component: () => import('../views/ContactView.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
