export default [{
        src: require('../assets/images/car-rent-app.png'),
        name: 'Car Rent App',
        description: 'این پروژه، یک سایت اجاره ی خودرو می باشد. که در آن میتوانید نوع ماشین اجاره ای خود، زمان تحویل گرفتن ماشین و زمان تحویل دادن آن را مشخص کنید و سفارش خود را به ثبت برسانید. برای راه اندازی این پروژه از Nuxt.js استفاده شده و برای مدیریت استیت ها، vuex به کار گرفته شده. وبسایت کاملا رسپانسیو و به صورت Mobile First طراحی شده. ***** برای ران کردن پروژه بعد از دستور npm run dev دستور npm run db:serve را برای اجرای json server جهت دریافت دیتا ها از local api اجرا کنید *****',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
            {
                src: require('../assets/images/tailwind.png'),
                name: 'Tailwind'
            },
            {
                src: require('../assets/images/vuejs.png'),
                name: 'Vue.js'
            },
            {
                src: require('../assets/images/nuxt.js.png'),
                name: 'Nuxt.js'
            },
            {
                src: require('../assets/images/vuex.png'),
                name: 'Vux'
            },
        ],
        links: [
            '/',
            'https://gitlab.com/Bahare_Ghaemi/car_rent-app'
        ]
    },
    {
        src: require('../assets/images/digitize-app.png'),
        name: 'Digitize App',
        description: 'این پروژه، یک سایت فروش لوازم دیجیتال می باشد. این سایت با vue.js توسعه داده شده است و برای مدیریت state ها از vuex استفاده شده است. امکانات این سایت عبارت است از: 1- امکان اضافه کردن محصولات به سبد خرید 2- گزینه های مختلف برای انتخاب محصول(رنگ و ..) 3- امکان اضافه کردن محصولات به علاقه مندی ها 4- امکان جست و جوی محصولات 5- امکان فیلتر کردن محصولات 6- رسپانسیو و mobile first بودن',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
            {
                src: require('../assets/images/tailwind.png'),
                name: 'Tailwind'
            },
            {
                src: require('../assets/images/vuejs.png'),
                name: 'Vue.js'
            },
            {
                src: require('../assets/images/vuex.png'),
                name: 'Vux'
            },
        ],
        links: [
            'https://digitize-app.vercel.app/',
            'https://gitlab.com/Bahare_Ghaemi/digitize-app'
        ]
    },
    {
        src: require('../assets/images/bahare-ghaemi.jpg'),
        name: 'My Resume',
        description: 'این پروژه وب سایت شخصی هست که با vuejs توسعه داده شده و برای Ui از tailwind استفاده شده است. برای ارتباط دادن تمام صفحات به یکدیگر هم از vue-router استفاده شده.',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
            {
                src: require('../assets/images/tailwind.png'),
                name: 'Tailwind'
            },
            {
                src: require('../assets/images/vuejs.png'),
                name: 'Vue.js'
            },
        ],
        links: [
            'https://bahare-ghaemi.vercel.app/',
            'https://gitlab.com/Bahare_Ghaemi/bahare-ghaemi'
        ]
    },
    {
        src: require('../assets/images/burger-wich.jpg'),
        name: 'Burger-wich fastfood',
        description: 'وبسایت برگرویچ با فریمورک vuejs توسعه داده شده و از tailwind در آن استفاده شده. از قابلیت های این app میتوان به ',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
            {
                src: require('../assets/images/tailwind.png'),
                name: 'Tailwind'
            },
            {
                src: require('../assets/images/vuejs.png'),
                name: 'Vue.js'
            },
        ],
        links: [
            'https://burgerwich-app.vercel.app/',
            'https://gitlab.com/Bahare_Ghaemi/fastfood-app'
        ]
    },
    {
        src: require('../assets/images/movie-app.png'),
        name: 'Movie App',
        description: 'این سایت، برترین فیلم های دنیا بر اساس امتیاز IMDB را نشان می دهد. لیست فیلم ها از API گرفته می شود و در قالب یکسری component، بر روی صفحه به نمایش گذاشته می شوند.',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
            {
                src: require('../assets/images/tailwind.png'),
                name: 'Tailwind'
            },
            {
                src: require('../assets/images/vuejs.png'),
                name: 'Vue.js'
            },
            {
                src: require('../assets/images/vuex.png'),
                name: 'Vux'
            },
            {
                src: require('../assets/images/nuxt.js.png'),
                name: 'Nuxt.js'
            },
        ],
        links: [
            '/',
            'https://gitlab.com/Bahare_Ghaemi/movie-app'
        ]
    },
    {
        src: require('../assets/images/school-profile.jpg'),
        name: 'School Profile',
        description: '',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
            {
                src: require('../assets/images/vuejs.png'),
                name: 'Vue.js'
            },
            {
                src: require('../assets/images/vuetify.png'),
                name: 'Vuetify'
            },
        ],
        links: [
            'https://school-profile.vercel.app/',
            'https://gitlab.com/Bahare_Ghaemi/school-profile'
        ]
    },
    {
        src: require('../assets/images/helium-plus.jpg'),
        name: 'Helium Plus',
        description: '',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
            {
                src: require('../assets/images/vuejs.png'),
                name: 'Vue.js'
            },
        ],
        links: [
            'https://helium-plus.vercel.app/',
            'https://gitlab.com/Bahare_Ghaemi/helium-plus'
        ]
    },
    {
        src: require('../assets/images/hoo-bank.jpg'),
        name: 'Hoo Bank',
        description: 'این نمونه تنها با html, css و tailwind ایجاد شده و کاملا رسپانسیو و mobilefirst است.',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
            {
                src: require('../assets/images/tailwind.png'),
                name: 'Tailwind'
            },
            {
                src: require('../assets/images/vuejs.png'),
                name: 'Vue.js'
            },
        ],
        links: [
            'https://hoobank-app.vercel.app/',
            'https://gitlab.com/Bahare_Ghaemi/hoobank-app'
        ]
    },
    {
        src: require('../assets/images/quize-app.png'),
        name: 'Quize App',
        description: 'این پروژه ی ',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
            {
                src: require('../assets/images/vuejs.png'),
                name: 'Vue.js'
            },
            {
                src: require('../assets/images/vuetify.png'),
                name: 'Vuetify'
            },
        ],
        links: [
            'https://quize-app-two.vercel.app/',
            'https://gitlab.com/Bahare_Ghaemi/quize-app'
        ]
    },
    {
        src: require('../assets/images/weather-app.jpg'),
        name: 'Weather App',
        description: 'پروژه ی هواشناسی که با html, css, sass, javascript کد نویسی شده است. اتها می توان اطلاعات شهرهای tehran,berlin,moscow,london را نمایش داد. اطلاعات توسط fetch از api هواشناسی گرفته می شود و با انتخاب شهر مورد نظر، آن اطلاعات نمایش داده میشود.',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
            {
                src: require('../assets/images/sass.png'),
                name: 'SASS'
            },
        ],
        links: [
            'https://weather-app-steel-five.vercel.app/',
            'https://gitlab.com/Bahare_Ghaemi/weather-app'
        ]
    },
    {
        src: require('../assets/images/contact-list-app.png'),
        name: 'Contact List App',
        description: '',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
        ],
        links: [
            'https://contact-list-app-murex.vercel.app/',
            'https://github.com/Bahare-Ghaemi/contact-list-app'
        ]
    },
    {
        src: require('../assets/images/carousel.png'),
        name: 'Carousel App',
        description: '',
        tools: [{
                src: require('../assets/images/html5.png'),
                name: 'HTML5'
            },
            {
                src: require('../assets/images/css3.png'),
                name: 'CSS3'
            },
            {
                src: require('../assets/images/javascript.png'),
                name: 'Javascript'
            },
        ],
        links: [
            'https://carousel-app-ten.vercel.app/',
            'https://gitlab.com/Bahare_Ghaemi/carousel'
        ]
    },
    {
        src: require('../assets/images/rezaeegroup.jpg'),
        name: 'Rezaee Group',
        description: 'این پروژه ی فروشگاهی، با وردپرس توسعه داده شده است و از افزونه هایی مانند افزونه فروشگاه ساز ووکامرس و افزونه ی صفحه ساز المنتور در آن استفاده شده است.',
        tools: [{
                src: require('../assets/images/wordpress.jpg'),
                name: 'wordpress'
            },
            {
                src: require('../assets/images/elementor.png'),
                name: 'elementor'
            },
            {
                src: require('../assets/images/woocommerce.jpg'),
                name: 'woocommerce'
            },
        ],
        links: [
            'https://rezaeegroup.com/',
            '#'
        ]
    },
]