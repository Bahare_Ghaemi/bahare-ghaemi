export default [
    {
        title: 'HTML5',
        bgLight: '#fecaca',
        bgDark: '#dc2626',
        value: '90%'
    },
    {
        title: 'CSS3',
        bgLight: '#bfdbfe',
        bgDark: '#2563eb',
        value: '80%'
    },
    {
        title: 'Flex & Grid',
        bgLight: '#fbcfe8',
        bgDark: '#db2777',
        value: '75%'
    },
    {
        title: 'SASS',
        bgLight: '#fbcfe8',
        bgDark: '#db2777',
        value: '70%'
    },
    {
        title: 'Bootstrap5',
        bgLight: '#ddd6fe',
        bgDark: '#7c3aed',
        value: '70%'
    },
    {
        title: 'Tailwind',
        bgLight: '#a5f3fc',
        bgDark: '#0891b2',
        value: '75%'
    },
    {
        title: 'Responsive (mobile first design)',
        bgLight: '#fecdd3',
        bgDark: '#e11d48',
        value: '80%'
    },
    {
        title: 'JavaScript',
        bgLight: '#fef08a',
        bgDark: '#ca8a04',
        value: '75%'
    },
    {
        title: 'Vue3',
        bgLight: '#a7f3d0',
        bgDark: '#059669',
        value: '75%'
    },
    {
        title: 'Vue-cli',
        bgLight: '#bbf7d0',
        bgDark: '#16a34a',
        value: '75%'
    },
    {
        title: 'Vue-router',
        bgLight: '#d9f99d',
        bgDark: '#65a30d',
        value: '75%'
    },
    {
        title: 'Vuetify',
        bgLight: '#bae6fd',
        bgDark: '#0284c7',
        value: '65%'
    },
    {
        title: 'Vuex',
        bgLight: '#99f6e4',
        bgDark: '#0d9488',
        value: '70%'
    },
    {
        title: 'Nuxt.js',
        bgLight: '#fbcfe8',
        bgDark: '#db2777',
        value: '65%'
    },
    {
        title: 'Wordpress',
        bgLight: '#c7d2fe',
        bgDark: '#4f46e5',
        value: '90%'
    },
    {
        title: 'Elementor',
        bgLight: '#fecdd3',
        bgDark: '#e11d48',
        value: '90%'
    },
    {
        title: 'Woocommerce',
        bgLight: '#f5d0fe',
        bgDark: '#c026d3',
        value: '80%'
    },
    {
        title: 'Git & Gitlab',
        bgLight: '#fed7aa',
        bgDark: '#ea580c',
        value: '70%'
    },
]